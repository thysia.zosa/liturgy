# Breviarium

## Ordinarium.

### Ad Vigilias.

* *Hymnus*.
* *Canticum*.
* Versus.
* Absolutio.
* *Lectio Apostolica* (**Batava**).
* (Cancticum Dan. 3,52-57.)
* (*Lectio Evangelica* **Batava**.)
* Te decet laus. / Te Deum.

### Ad Matutinos.

* *Tres Psalmi* (**Batava**).
* *Capitulum*.
* *Hymnus*.
* *Versus*.
* Benedictus cum *Antiphona*.
* Preces Matutinæ.
* *Oratio*.

### Ad Primam.

* Hymnus.
* Capitulum.
* Responsorium.
* Versus.
* Preces diei.
* Oratio.
* Officium Capituli:
	* Benedictio.
	* *Lectio Regularis* (**Batava**).
	* Versus.
	* Confessio.
	* Benedictio.

### Ad Tertiam, Sextam et Nonam.

* Hymnus.
* *Capitulum*.
* *Responsorium*.
* *Versus*.
* Preces diurnæ.

### Ad Vesperas.

* *Tres Psalmi* (**Batava**). 
* *Capitulum*.
* *Hymnus*.
* *Versus*.
* Magnificat cum *Antiphona*.
* Preces Vespertinæ.
* *Oratio*.

### Ad Completorium.

* Officium in fine diei:
	* Benedictio.
	* *Lectio Prophetica* (**Batava**).
	* Versus.
	* Confessio.
	* Indulgentia.
* Hymnus.
* Capitulum.
* Responsorium.
* Versus.
* Nunc dimittis cum Antiphona.
* Preces Nocturnæ.
