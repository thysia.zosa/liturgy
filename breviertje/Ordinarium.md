# Ordinarium
## Ad Matutinum
**V.** Dómine, lábia mea apéries.  
**R.** Et os meum annuntiábit laudem tuam.
**V.** Deus, in adiutórium meum inténde.  
**R.** Dómine, ad adiuvándum me festína.  
Glória Patri, et Fílio, et Spirítui Sancto.  
Sicut erat in princípio, et nunc et semper, * et in sǽcula sæculórum. Amen.  
Alleluia.  
*vel*  
Laus tibi, Dómine, Rex ætérnæ glóriæ.  

### Psalmus 94
Veníte, exsultémus Dómino; * iubilémus Deo salutári nostro.  
Præoccupémus fáciem eius in confessióne * et in psalmis iubilémus ei.  
Quóniam Deus magnus Dóminus, * et rex magnus super omnes deos.  
Quia in manu eius sunt profúnda terræ, * et altitúdines móntium ipsíus sunt.  
Quóniam ipsíus est mare, et ipse fecit illud, * et siccam manus eius formavérunt.  
Veníte, adorémus et procidámus * et génua flectámus ante Dóminum, qui fecit nos,  
quia ipse est Deus noster, * et nos pópulus pascuæ eius et oves manus eius.  
Utinam hódie vocem eius audiátis: * Nolíte obduráre corda vestra,  
sicut in Meríba, secúndum diem Massa in desérto, † ubi tentavérunt me patres vestri: * probaverunt me, etsi vidérunt ópera mea.  
Quadragínta annis tǽduit me generatiónis illíus * et dixi: Pópulus errántium corde sunt isti.  
Et ipsi non cognovérunt vias meas; † ídeo iurávi in ira mea: * Non introíbunt in réquiem meam.  

### *Hymnus -- Psalmodia -- Versus*
### Absolutio

### Lectiones cum benedictionibus

### Hymnus Te Deum
Te Deum laudámus: * te Dóminum confitémur.  
Te ætérnum Patrem, * omnis terra venerátur.  
Tibi omnes ángeli, * tibi cæli et univérsæ potestátes:  
tibi chérubum et séraphim * incessábili voce proclámant:  
Sanctus, sanctus, sanctus * Dóminus Deus Sábaoth.  
Pleni sunt cæli et terra * maiestátis glóriæ tuæ.  
Te gloriósus * Apostolórum chorus,  
te prophetárum * laudábilis númerus,  
te mártyrum candidátus * laudat exércitus.  
Te per orbem terrárum * sancta confitétur Ecclésia,  
Patrem * imménsæ maiestátis;  
venerándum tuum verum * et únicum Fílium;  
Sanctum quoque * Paráclitum Spíritum

## Ad Laudes

## Ad Primam

## Ad Tertiam

## Ad Sextam

## Ad Nonam

## Ad Vesperas

## Ad Completorium
