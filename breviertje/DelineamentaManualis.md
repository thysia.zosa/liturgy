# Breviarium

## Ordinarium.

### Ad Matutinam.

* *Hymnus*.

### Ad Laudes.

* *Canticum*.
* *Tres Psalmi* (**Batava**).
* *Capitulum*.
* *Hymnus*.
* *Versus*.
* Benedictus cum *Antiphona*.
* Preces Matutinæ.
* *Oratio*.

### Ad Primam.

* Hymnus.
* Capitulum.
* Responsorium.
* Versus.
* Preces diei.
* Oratio.
* Officium Capituli:
	* Benedictio.
	* *Lectio Apostolica* (**Batava**).
	* (Cancticum Dan. 3,52-57.)
	* (*Lectio Evangelica* **Batava**.)
	* (Te Deum.)
	* Versus.
	* Confessio.
	* Versus.
	* Absolutio.
	* Præfatio.
	* Benedictio.

### Ad Tertiam, Sextam et Nonam.

* Hymnus.
* *Capitulum*.
* *Responsorium*.
* *Versus*.
* Preces diurnæ.

### Ad Vesperas.

* *Tres Psalmi* (**Batava**).
* *Capitulum*.
* *Hymnus*.
* *Versus*.
* Magnificat cum *Antiphona*.
* Preces Vespertinæ.
* *Oratio*.

### Ad Completorium.

* Officium in fine diei:
	* Benedictio.
	* *Lectio Prophetica* (**Batava**).
	* Versus.
	* Confessio.
	* Indulgentia.
* Hymnus.
* Capitulum.
* Responsorium.
* Versus.
* Nunc dimittis cum Antiphona.
* Preces Nocturnæ.
