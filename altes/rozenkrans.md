[Rozenkranswebsite](https://gebeden-site.jouwweb.nl/rozenhoedjes/info-over-de-rozenhoedjes)

In de naam van de Vader, en de Zoon, en de heilige Geest. Amen.

Ik geloof in God, de almachtige vader, schepper van hemel en aarde.  
En in Jezus Christus, zijn enige zoon, onze Heer. Die ontvangen is van de heilige Geest, geboren uit de maagd Maria; die geleden heeft onder Pontius Pilatus, is gekruisigd, gestorven en begraven; die neergedaald is ter helle, de derde dag verrezen uit de doden; die opgestegen is ten hemel, zit aan de rechterhand van God, de almachtige vader; vandaar zal hij komen oordelen de levenden en de doden.  
Ik geloof in de heilige Geest; de heilige katholieke kerk; de gemeenschap van de heiligen; de vergeving van de zonden; de verrijzenis van het lichaam; en het eeuwig leven. Amen.  

Onze vader, die in de hemel zijt, uw naam worde geheiligd; uw rijk kome; uw wil geschiede op aarde zoals in de hemel. Geef ons heden ons dagelijks brood; en vergeef ons onze schuld, zoals ook wij aan anderen hun schuld vergeven; en leid ons niet in bekoring, maar verlos ons van het kwade.  
Want van u is het koninkrijk, en de kracht, en de heerlijkheid in eeuwigheid. Amen.

Wees gegroet, Maria, vol van Genade, De Heer is met u; gij zijt de gezegende onder de vrouwen, en gezegend is Jezus, de vrucht van uw schoot.  
Heilige Maria, Moeder van God, bid voor ons, zondaars, nu en in het uur van onze dood. Amen.

Eer aan de vader, en de zoon, en de heilige Geest.  
Zoals het was in het begin, en nu en altijd en in de eeuwen der eeuwen. Amen.

O mijn Jezus, vergeef ons onze zonden, behoed ons voor het vuur van de hel, breng alle zielen naar de hemel, vooral diegenen die uw barmhartigheid het meeste nodig hebben.


Blijde geheimen:  
	- De engel Gabriël brengt de blijde boodschap an Maria.  
	- Maria bezoekt haar nicht Elisabeth.  
	- Jezus wordt geboren in de stal van Bethlehem.  
	- Jezus wordt in de tempel opgedragen.  
	- Jezus wordt in de tempel teruggevonden.

Geheimen van het licht:  
	- De doop van Jezus in de Jordaan.  
	- De openbaring van Jezus op de bruiloft van Kana.  
	- Jezus' aankondiging van het Rijk Gods.  
	- De gedaanteverandering van Jezus op de berg Tabor.  
	- Jezus stelt de Eucharistie in.  

Droevige geheimen:  
	- Jezus bidt in doodsangst tot zijn hemelse vader.  
	- Jezus wordt gegeseld.  
	- Jezus wordt met doornen gekroond.  
	- Jezus draagt het kruis naar de berg van Calvarië.  
	- Jezus sterft aan het kruis.  

Glorievolle geheimen:  
	- Jezus verrijst uit de doden.  
	- Jezus stijgt op ten hemel.  
	- De heilige Geest daalt neer over de apostelen.  
	- Maria wordt in de hemel opgenomen.  
	- Maria wordt in de hemel gekroond.  


Wees gegroet, koningin, moeder van barmhartigheid; ons leven, onze vreugde en onze hoop, wees gegroet. Tot u roepen wij, ballingen, kinderen van Eva; tot u smeken wij, zuchtend en wenend in dit dal van tranen.  
Daarom dan, onze voorspreekster, sla op ons uw barmhartige ogen; en toon ons, na deze ballingschap, Jezus, de gezegende vrucht van uw schoot.  
O goedertieren, o liefdevolle, o zoete maagd Maria.

Bid voor ons, heilige moeder van God.  
Opdat wij de beloften van Christus waardig mogen worden.  
Laat ons bidden.  
God, uw eniggeboren zoon heeft ons door zijn leven, zijn dood en verrijzenis de beloning van het eeuwig heil bereid. Wij vragen u, geef ons, die door de allerheiligste rozenkrans van de zalige maagd Maria deze geheimen gedenken, dat wij mogen navolgen wat zij bevatten, en mogen verkrijgen wat zij beloven. Door Christus, onze Heer. Amen.






---
Eeuwige vader, ik offer u op het lichaam en bloed, de ziel en de godheid van uw welbeminde zoon, onze Heer Jezus Christus, tot verzoening van onze zonden en die zonden van heel de wereld.

Door het smartelijk lijden van uw zoon, heb medelijden met ons en met heel de wereld.

Heilige God, heilige sterke God, heilige onsterfelijke God, heb erbarmen met ons en met heel de wereld.
